<?php

function GetRequest($i){
  $R = "";
  if(isset(array_values(array_filter(explode("/", urldecode($_SERVER['REQUEST_URI']))))[$i])){
    $R = array_values(array_filter(explode("/", urldecode($_SERVER['REQUEST_URI']))))[$i];
  }
  return $R;
}

function curl_request_get($url){
  $curl = curl_init();
  curl_setopt_array($curl, [
      CURLOPT_URL => $url
  ]);
  $resp = curl_exec($curl);
  curl_close($curl);
}


$request = strtok(GetRequest(0), '?');
switch($request){
  case "insurance":
    switch(strtok(GetRequest(1), '?')){  
      case "travel":
        $page_title = "Reiseforsikring | Dokumentarkiv";
        include_once('views/insurance-travel.php');
      break;
      case "idtheft":
      $page_title = "Identitetstyveriforsikring | Dokumentarkiv";
      include_once('views/insurance-idtheft.php');
    break;
      default:    
        $page_title = "Forsikring | Dokumentarkiv";
        include_once('views/insurance.php');
      break;
    }
  break;
  default:
  $page_title = "Dokumentarkiv";
  include_once('views/home.php');
  break;
}