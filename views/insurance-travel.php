<?php 
  require_once('parts/header.php');
?>


  <!-- About -->
  <section class="page-section" id="forsikringer">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Reiseforsikring</h2>
          <p>Dokumentene på denne siden gjelder for reiseforsikring tilbydt av DealPass.</p>

          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Dokumenter</a></li>
            <li class="breadcrumb-item"><a href="/insurance">Forsikring</a></li>
            <li class="breadcrumb-item active" aria-current="page">Reise</li>
            </ol>
          </nav>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <p>
            <div class="list-group">
                <a target="_blank" class="list-group-item list-group-item-action pdf-file" href="/files/insurance/travel/Gouda-Norge_637_Medlemskort_reiseforsikring-Dealpass_jul19.pdf">Forsikringsavtale</a>
                <a target="_blank" class="list-group-item list-group-item-action pdf-file" href="/files/insurance/travel/Gouda-Norge_A4_Forsikringsbevis_DealPass-Medlemskort_jul19.pdf">Forsikringsbevis</a>
                <a target="_blank" class="list-group-item list-group-item-action pdf-file" href="/files/insurance/travel/Gouda-Norge-DealPass_Medlemskort_Safety-Card_jul19.pdf">Forsikringskort</a>
                <a target="_blank" class="list-group-item list-group-item-action pdf-file" href="/files/insurance/travel/Gouda-Norge_Skademelding-kortkunder_jul19_formular.pdf">Skademeldingsskjema</a>
            </div>
          </p>
        </div>
      </div>
    </div>
  </section>

  <?php 
  require_once('parts/footer.php');
?>