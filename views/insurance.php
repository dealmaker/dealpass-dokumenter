<?php 
  require_once('parts/header.php');
?>

  <!-- About -->
  <section class="page-section" id="forsikringer">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Forsikringsdokumenter</h2>
          <p>På disse sidene vil du finne viktige dokumenter til forsikringer gjennom DealPass.</p>

          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Dokumenter</a></li>
            <li class="breadcrumb-item active" aria-current="page">Forsikring</li>
            </ol>
          </nav>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <p>
            <div class="list-group">
              <!-- <a class="list-group-item list-group-item-action folder" href="/general">Brukervilkår og Personvern</a> -->
              <a class="list-group-item list-group-item-action folder" href="/insurance/travel">Forsikring - Reise</a>
              <a class="list-group-item list-group-item-action folder" href="/insurance/idtheft">Forsikring - ID-tyveri</a>
            </div>
          </p>
        </div>
      </div>
    </div>
  </section>

<?php 
  require_once('parts/footer.php');
?>