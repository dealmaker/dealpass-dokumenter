<?php 
  require_once('parts/header.php');
?>


  <!-- About -->
  <section class="page-section" id="forsikringer">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">ID-tyveriforsikring</h2>
          <p>Dokumentene på denne siden gjelder for ID-tyveriforsikring tilbydt av DealPass.</p>

          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Dokumenter</a></li>
            <li class="breadcrumb-item"><a href="/insurance">Forsikring</a></li>
            <li class="breadcrumb-item active" aria-current="page">Reise</li>
            </ol>
          </nav>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <p>
            <div class="list-group">
                <a target="_blank" class="list-group-item list-group-item-action pdf-file" href="/files/insurance/idtheft/Gouda-Norge_680_ID-tyverisikring-DealPass_jul19.pdf">Forsikringsavtale</a>
            </div>
          </p>
        </div>
      </div>
    </div>
  </section>

  <?php 
  require_once('parts/footer.php');
?>