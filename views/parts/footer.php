
  <!-- Footer -->
  <footer class="footer">
    <div class="container">
      <div class="row align-items-center">

        <div class="col-md-12 info">
          Vi hører gjerne fra deg, ring oss på kundeservice: 417 99 900 (mandag-fredag 09-15) post@dealpass.no (24/7) – Vakttelefon: 949 70 402 <br/>
          Tjenesten er administrert av Dealmaker AS (996645738 MVA)<br />
          Karmsundgaten 150, 5527 Haugesund
        </div>

      </div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="/vendor/jquery/jquery.min.js"></script>
  <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Contact form JavaScript -->
  <script src="/vendor/jqBootstrapValidation/jqBootstrapValidation.js"></script>
  <script src="/vendor/dealpass/js/contact_me.js"></script>

  <!-- Custom scripts for this template -->
  <script src="/vendor/dealpass/js/agency.min.js"></script>

</body>

</html>
