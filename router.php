<?php

ini_set('display_errors', 'On');
error_reporting(E_ALL);

if (file_exists(__DIR__ . '/' . $_SERVER['REQUEST_URI'])) {
   return false; // serve the requested resource as-is.
}
else {
    include_once 'index.php';
}
